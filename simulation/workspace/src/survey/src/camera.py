#!/usr/bin/env python3

import rclpy
from rclpy.node import Node
from sensor_msgs.msg import Image
from rclpy.qos import qos_profile_sensor_data
import cv2
from pyzbar.pyzbar import decode
from cv_bridge import CvBridge, CvBridgeError
import numpy as np

class Camera_subscriber(Node):

    def __init__(self):
        super().__init__('mypkg_subscriber')
        self.subscription = self.create_subscription(
			Image,
			'/camera/image_raw',
			self.listener_callback,
			qos_profile_sensor_data)
        self.subscription  # prevent unused variable warning
        self.bridge = CvBridge()

    def listener_callback(self, image):
        cv_image =outres = self.bridge.imgmsg_to_cv2(image, desired_encoding='bgr8')
        try: print(decode(outres)[0].data.decode('UTF-8'))
        except: print('no qr')

def main(args=None):
    rclpy.init(args=args)
    camera = Camera_subscriber()
    rclpy.spin(camera)
    camera.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()

