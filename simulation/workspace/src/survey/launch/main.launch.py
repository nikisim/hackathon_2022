from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import IncludeLaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.actions import DeclareLaunchArgument
import time
from launch_ros.substitutions import FindPackageShare
from launch.actions import TimerAction

def generate_launch_description():
    ld = LaunchDescription()
    package_prefix_nav2_bringup = FindPackageShare(package='nav2_bringup').find('nav2_bringup')
    package_prefix_slam_toolbox = FindPackageShare(package='slam_toolbox').find('slam_toolbox')
    package_prefix_survey = get_package_share_directory('survey')
    

    DeclareLaunchArgument(
            'slam_params_file',
            default_value = '/workspace/src/survey/config/nav2_params.yaml',
            description = 'Parameters file for nav2'),
    DeclareLaunchArgument(
            'params_file',
            default_value = '/workspace/src/survey/config/nav2_params.yaml',
            description = 'Parameters file for nav2'),
    start_world = IncludeLaunchDescription(
            PythonLaunchDescriptionSource([package_prefix_survey, '/launch/ozyland.launch.py'])
            )
    start_slam_toolbox = IncludeLaunchDescription(
            PythonLaunchDescriptionSource([package_prefix_slam_toolbox, '/launch/online_async_launch.py']),
            launch_arguments = {'slam_params_file':'/workspace/src/survey/config/nav2_params.yaml'}.items()
            )
    
    start_nav2_bringup= IncludeLaunchDescription(
            PythonLaunchDescriptionSource([package_prefix_nav2_bringup, '/launch/navigation_launch.py']),
            launch_arguments = {'params_file':'/workspace/src/survey/config/nav2_params.yaml'}.items()
            )
    
    start_slam_toolbox = TimerAction(period=5.0, actions=[start_slam_toolbox])    
    start_nav2_bringup = TimerAction(period=5.0, actions=[start_nav2_bringup])    
    ld.add_action(start_world)
    ld.add_action(start_slam_toolbox)
    ld.add_action(start_nav2_bringup)
    return ld
    
